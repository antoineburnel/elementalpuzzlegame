using UnityEngine;

[CreateAssetMenu(fileName = "XYZ_Dimension", menuName = "ScriptableObjects/New Dimension", order = 0)]
public class DimensionData : ScriptableObject
{
    [SerializeField] private byte _id;
    [SerializeField] private string _fullname;
    [SerializeField] private float _global2DLightIntensity;
    [SerializeField] private Color _backgroundColor;
    [SerializeField] private float _globalYOffset;

    public byte GetId()
    {
        return _id;
    }

    public string GetFullname()
    {
        return _fullname;
    }

    public float GetGlobal2DLightIntensity()
    {
        return _global2DLightIntensity;
    }

    public Color GetCameraBackgroundColor()
    {
        return _backgroundColor;
    } 

    public float GetYGlobalOffset()
    {
        return _globalYOffset;
    }

    public bool Equals(DimensionData otherDimension)
    {
        return _id == otherDimension._id;
    }
}