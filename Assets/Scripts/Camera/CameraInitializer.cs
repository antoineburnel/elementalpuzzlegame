using UnityEngine;

public class CameraInitializer : MonoBehaviour
{
    [SerializeField] private Room _room;

    private void Start()
    {
        GetComponent<CameraController>().Goto(_room, 0f);
    }
}