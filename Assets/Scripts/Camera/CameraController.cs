using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera _camera;
    private Transform _cameraTransform;


    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _cameraTransform = transform;
    }

    public void Goto(Room room, float transitionDuration)
    {
        StopTime();
        StartCoroutine(GoToPositionIn(room.GetCameraPosition(), transitionDuration));
        StartCoroutine(ResumeTimeAfter(transitionDuration));
    }

    private void StopTime()
    {
        Time.timeScale = 0f;
    }

    private IEnumerator ResumeTimeAfter(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        Time.timeScale = 1f;
    }

    private IEnumerator GoToPositionIn(Vector3 position, float duration)
    {
        Vector3 startPos = _cameraTransform.localPosition;
        Vector3 endPos = position;

        float elapsed = 0f;
        float ratio;

        while (elapsed < duration)
        {
            elapsed += Time.unscaledDeltaTime;
            ratio = elapsed / duration;

            _cameraTransform.localPosition = Vector3.LerpUnclamped(startPos, endPos, Easing.EaseInOutQuart(ratio));

            yield return null;
        }

        _cameraTransform.localPosition = endPos;
    }
}