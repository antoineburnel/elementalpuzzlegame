using UnityEngine;

public class PlayerEventManager : MonoBehaviour
{
    public delegate void OnRoomSwitch(RoomSwitchData data);
    public OnRoomSwitch onRoomSwitch;
    public void NotifyRoomSwitch(RoomSwitchData data) { onRoomSwitch?.Invoke(data); }

    public delegate void OnRoomSwitchOver();
    public OnRoomSwitchOver onRoomSwitchOver;
    public void NotifyRoomSwitchOver() { onRoomSwitchOver?.Invoke(); }

    public delegate void OnDimEntered(DimensionData data);
    public OnDimEntered onDimEntered;
    public void NotifyEnteringDim(DimensionData data) { onDimEntered?.Invoke(data); }

    public delegate void OnDimEnteredOver();
    public OnDimEnteredOver onDimEnteredOver;
    public void NotifyEnteredDimOver() { onDimEnteredOver?.Invoke(); }
}