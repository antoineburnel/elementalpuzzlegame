using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    [SerializeField] private CameraController _camera;

    private bool _isSwitchingRoom = false;

    private void Awake()
    {
        GetComponent<PlayerEventManager>().onRoomSwitch += OnRoomSwitch;
        GetComponent<PlayerEventManager>().onRoomSwitchOver += OnRoomSwitchOver;
    }

    private void OnDestroy()
    {
        GetComponent<PlayerEventManager>().onRoomSwitch -= OnRoomSwitch;
        GetComponent<PlayerEventManager>().onRoomSwitchOver -= OnRoomSwitchOver;
    }

    private void OnRoomSwitch(RoomSwitchData data)
    {
        if (_isSwitchingRoom) return;
        _isSwitchingRoom = true;
        _camera.Goto(data.GetNewRoom(), data.GetTransitionDuration());
    }

    private void OnRoomSwitchOver()
    {
        _isSwitchingRoom = false;
    }
}