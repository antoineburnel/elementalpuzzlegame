using System.Collections;
using UnityEngine;

public class PlayerMovementUpdater : MonoBehaviour
{
    private Transform _player;
    private Rigidbody2D _rb;
    [SerializeField] private float _crouchingSpeed;
    [SerializeField] private float _walkingSpeed;
    [SerializeField] private float _runningSpeed;
    [SerializeField] private float _jumpVelocity;
    [SerializeField] private float _jumpRaycastDistance;
    private bool _lookingLeft = false;
    private bool _canInterruptJump = false;
    private bool _bufferedJump = false;
    private static float BUFFERED_JUMP_DURATION = 0.25f;
    private Coroutine _unbufferJumpCoroutine = null;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _player = transform;
    }

    public void OnUpdate(bool wantToJump, bool releasedJumpButton)
    {
        if (releasedJumpButton && CanInterruptJump()) InterruptJump();
        if ((wantToJump || _bufferedJump) && CanJump()) Jump();
        if (wantToJump && !CanJump()) BufferJump();
    }

    public void GotoPositionIn(Vector3 position, float duration)
    {
        StartCoroutine( LerpToPositionIn(position, duration) );
    }

    private IEnumerator LerpToPositionIn(Vector3 position, float duration)
    {
        Vector3 startPos = _player.position;
        Vector3 endPos = position;

        float elapsed = 0f;
        float ratio;

        while (elapsed < duration)
        {
            elapsed += Time.unscaledDeltaTime;
            ratio = elapsed / duration;

            _player.position = Vector3.LerpUnclamped(startPos, endPos, ratio);

            yield return null;
        }

        _player.position = endPos;
    }

    public void OnFixedUpdate(float inputX, bool wantToCrouch, bool wantToRun)
    {
        UpdatePlayerHorizontalVelocity(inputX, wantToCrouch, wantToRun);
        UpdatePlayerFacingDirection(inputX);
    }

    private void UpdatePlayerHorizontalVelocity(float inputX, bool wantToCrouch, bool wantToRun)
    {
        float currentSpeed = GetPlayerVelocity(wantToCrouch, wantToRun);
        _rb.velocity = new Vector2(inputX * currentSpeed * Time.fixedDeltaTime, _rb.velocity.y);
    }

    private void UpdatePlayerFacingDirection(float inputX)
    {
        if (inputX > 0 && _lookingLeft)
        {
            _lookingLeft = false;
            _player.localScale = new Vector3(-_player.localScale.x, 1f, 1f);
        }
        else if (inputX < 0 && !_lookingLeft)
        {
            _lookingLeft = true;
            _player.localScale = new Vector3(-_player.localScale.x, 1f, 1f);
        }
    }

    private void Jump()
    {
        _canInterruptJump = true;
        _bufferedJump = false;
        _rb.velocity = new Vector2(_rb.velocity.x, _jumpVelocity);
    }

    private void InterruptJump()
    {
        _canInterruptJump = false;
        _rb.velocity = new Vector2(_rb.velocity.x, 0f);
    }

    private float GetPlayerVelocity(bool wantToCrouch, bool wantToRun)
    {
        if (wantToCrouch) return _crouchingSpeed;
        if (wantToRun) return _runningSpeed;
        return _walkingSpeed; 
    }

    private void BufferJump()
    {
        _bufferedJump = true;
        if (_unbufferJumpCoroutine != null) StopCoroutine(_unbufferJumpCoroutine);
        _unbufferJumpCoroutine = StartCoroutine(UnbufferJumpAfter(BUFFERED_JUMP_DURATION));
    }

    private IEnumerator UnbufferJumpAfter(float delay)
    {
        yield return new WaitForSeconds(delay);
        _bufferedJump = false;
    }

    private bool CanJump()
    {
        return IsGrounded();
    }

    private bool CanInterruptJump()
    {
        return _canInterruptJump && !IsFalling() && !IsGrounded();
    }

    private bool IsFalling()
    {
        return _rb.velocity.y < 0f;
    }

    private bool IsGrounded()
    {
        LayerMask layerMask = LayerMask.GetMask("Ground");
        RaycastHit2D hit = Physics2D.Raycast(_player.position, Vector2.down, _jumpRaycastDistance, layerMask);
        bool IsNotOnTheGround = hit.collider == null;
        return !IsNotOnTheGround;
    }
}