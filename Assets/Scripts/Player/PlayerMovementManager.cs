using System.Collections;
using UnityEngine;

[RequireComponent(typeof(PlayerMovementUpdater))]

public class PlayerMovementManager : MonoBehaviour
{
    private Transform _player;
    private PlayerMovementUpdater _playerMovementUpdater;
    private float _inputX;
    private bool _wantToCrouch;
    private bool _wantToRun;
    private bool _wantToJump;
    private bool _releasedJumpButton;

    private bool _canMove = true;
    private bool _isSwitchingRoom = false;

    private void Awake()
    {
        _player = transform;
        GetComponent<PlayerEventManager>().onRoomSwitch += OnRoomSwitch;
        GetComponent<PlayerEventManager>().onRoomSwitchOver += OnRoomSwitchOver;
        _playerMovementUpdater = GetComponent<PlayerMovementUpdater>();
    }

    private void OnDestroy()
    {
        GetComponent<PlayerEventManager>().onRoomSwitch -= OnRoomSwitch;
        GetComponent<PlayerEventManager>().onRoomSwitchOver -= OnRoomSwitchOver;
    }

    private void Update()
    {
        _inputX = Input.GetAxis("Horizontal");
        _wantToRun = Input.GetKey(KeyCode.LeftShift);
        _wantToCrouch = Input.GetKey(KeyCode.LeftControl);
        _wantToJump = Input.GetKeyDown(KeyCode.Space);
        _releasedJumpButton = Input.GetKeyUp(KeyCode.Space);

        if (_canMove) _playerMovementUpdater.OnUpdate(_wantToJump, _releasedJumpButton);
    }

    private void FixedUpdate()
    {
        if (_canMove) _playerMovementUpdater.OnFixedUpdate(_inputX, _wantToCrouch, _wantToRun);
    }

    private void OnRoomSwitch(RoomSwitchData data)
    {
        if (_isSwitchingRoom) return;
        _isSwitchingRoom = true;
        StopMovement();
        Vector3 newPosition = new Vector3(data.GetNewPlayerPosition().x, transform.position.y, transform.position.z);
        _playerMovementUpdater.GotoPositionIn(newPosition, data.GetTransitionDuration());
        StartCoroutine( ReactivateMovementAfter(data.GetTransitionDuration()) );
    }

    private void OnRoomSwitchOver()
    {
        _isSwitchingRoom = false;
    }

    private void StopMovement()
    {
        _canMove = false;
    }

    private IEnumerator ReactivateMovementAfter(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        _canMove = true;
    }
}
