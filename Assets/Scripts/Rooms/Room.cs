using UnityEngine;

public class Room : MonoBehaviour
{
    private Vector3 _cameraPosition;
    [SerializeField] private Room _equivalentRoomInOtherDimension;

    private void Awake()
    {
        _cameraPosition = transform.position;
    }

    public Vector3 GetCameraPosition()
    {
        return _cameraPosition;
    }

    public Room GetQuivalentRoomButInAnotherDimension()
    {
        return _equivalentRoomInOtherDimension;
    }
}