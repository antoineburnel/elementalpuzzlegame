using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]

public class RoomSwitchData : MonoBehaviour
{
    [SerializeField] private Room _newRoom;
    [SerializeField] private Transform _newPlayerTransform;
    [SerializeField] private float _transitionDuration = 0.5f;

    private PlayerEventManager _playerEventSystem;

    private void Awake()
    {
        if (_newPlayerTransform == null) _newPlayerTransform = transform.GetChild(0);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;

        Transform player = other.GetComponent<Parent>().GetParent();
        _playerEventSystem = player.GetComponent<PlayerEventManager>();
        _playerEventSystem.NotifyRoomSwitch(this);
        StartCoroutine( NotifyRoomSwitchOverAfter(_transitionDuration) );
    }

    private IEnumerator NotifyRoomSwitchOverAfter(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        _playerEventSystem.NotifyRoomSwitchOver();
    }

    public Room GetNewRoom()
    {
        return _newRoom;
    }

    public Vector3 GetNewPlayerPosition()
    {
        return _newPlayerTransform.position;
    }

    public float GetTransitionDuration()
    {
        return _transitionDuration;
    }
}