using UnityEngine;

public class Parent : MonoBehaviour
{
    [SerializeField] private Transform _parent;

    public Transform GetParent()
    {
        return _parent;
    }
}