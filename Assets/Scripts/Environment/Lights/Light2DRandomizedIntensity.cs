using UnityEngine;
using UnityEngine.Rendering.Universal;

[RequireComponent(typeof(Light2D))]

public class Light2DRandomizedIntensity : MonoBehaviour
{
    private Light2D _light2D;
    [SerializeField] private Vector2 _intensityLimits = new Vector2(0, 1);
    [SerializeField] private float _delayBetweenRandomIntensity = 0.25f;

    private void Awake()
    {
        _light2D = GetComponent<Light2D>();
    }

    private void Start()
    {
        GenerateNewIntensityIn(_delayBetweenRandomIntensity);
    }

    private void GenerateNewIntensity()
    {
        float newIntensity = Random.Range(_intensityLimits.x, _intensityLimits.y);
        _light2D.intensity = newIntensity;
        GenerateNewIntensityIn(_delayBetweenRandomIntensity);
    }

    private void GenerateNewIntensityIn(float delay)
    {
        Invoke("GenerateNewIntensity", delay);
    }
}
