using UnityEngine;

public static class Easing
{
    public static float EaseInOutQuart(float x)
    {
        if (x < 0.5) return 8 * x * x * x * x; 
        return 1 - Mathf.Pow(-2 * x + 2, 4) / 2;
    }
}